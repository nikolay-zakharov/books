<?php

Route::group(['namespace' => '\App\Http\Controllers\Api'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::group(['middleware' => 'auth.jwt'], function () {
        Route::post('logout', 'AuthController@logout');

        Route::group(['prefix' => 'authors'], function () {
            Route::post('/', 'AuthorsController@add');
            Route::get('{id}/books', 'BooksController@getListByAuthor')->where('id', '\d+');
        });

        Route::group(['prefix' => 'books'], function () {
            Route::get('/', 'BooksController@getList');
            Route::post('/', 'BooksController@add');
            Route::get('{id}', 'BooksController@getOne')->where('id', '\d+');
            Route::patch('{id}', 'BooksController@update')->where('id', '\d+');
        });
    });
});
