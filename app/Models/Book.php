<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property int id
 * @property string title
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property Author[]|Collection authors
 * @see Book::authors()
 */
class Book extends Model
{
    protected $visible = ['id', 'title', 'authors'];

    protected $fillable = ['title', 'authors'];

    public function authors()
    {
        return $this->belongsToMany(Author::class, 'books_authors');
    }
}
