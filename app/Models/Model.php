<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

abstract class Model extends EloquentModel
{
    public static function getTableName()
    {
        return (new static)->getTable();
    }

    public function toJson($options = 0)
    {
        return parent::toJson(JSON_UNESCAPED_UNICODE);
    }
}
