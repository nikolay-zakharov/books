<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property int id
 * @property string name
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property Book[]|Collection books
 * @see Author::books()
 */
class Author extends Model
{
    protected $visible = ['id', 'name', 'books'];

    protected $fillable = ['name'];

    public function books()
    {
        return $this->belongsToMany(Book::class, 'books_authors');
    }
}
