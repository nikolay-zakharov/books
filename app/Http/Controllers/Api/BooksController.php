<?php

namespace App\Http\Controllers\Api;

use App\Models\Author;
use App\Models\Book;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Log;

class BooksController extends Controller
{
    public function getList(Request $request)
    {
        /** @var Builder $booksQuery */
        $booksQuery = Book::query()
            ->orderBy('id');

        return $this->getPaginated($booksQuery, $request, function (Collection $books, array $listing_info) {
            return response()->json($books, 200, $listing_info);
        });
    }

    public function getListByAuthor(Request $request, int $id)
    {
        /** @var Author $author */
        $author = Author::findOrFail($id);

        /** @var Builder $booksQuery */
        $booksQuery = Book::query()
            ->whereHas('authors', function ($query) use ($author) {
                $query->where(Author::getTableName().'.id', $author->id);
            })
            ->orderBy(Book::getTableName().'.id');

        return $this->getPaginated($booksQuery, $request, function (Collection $books, array $listing_info) {
            return response()->json($books, 200, $listing_info);
        });
    }

    public function getOne(int $id)
    {
        /** @var Book $book */
        $book = Book::findOrFail($id);
        $book->load(['authors']);

        return response()->json($book);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:255',
            'authors' => 'array',
            'authors.*' => 'int|exists:authors,id',
        ]);

        try {
            /** @var Book $book */
            $book = DB::transaction(function () use ($request) {
                $book = Book::create($request->only(['title']));

                if ( ! empty($request->get('authors'))) {
                    $book->authors()->sync($request->get('authors'));
                }

                return $book;
            });

            $book->load(['authors']);

            return response()->json([
                'success' => true,
                'book' => $book,
            ]);
        }
        catch (Exception $e) {
            Log::error('Failed to add Book: '.$e->getMessage().PHP_EOL.'Input: '.print_r($request->except(['token']), true).PHP_EOL.$e->getTraceAsString());

            return response()->json(['success' => false], 500);
        }
    }

    public function update(Request $request, int $id)
    {
        /** @var Book $book */
        $book = Book::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|string|max:255',
            'authors' => 'array',
            'authors.*' => 'int|exists:authors,id',
        ]);

        try {
            DB::transaction(function () use ($book, $request) {
                $book->title = $request->get('title');
                $book->save();

                if ($request->exists('authors')) {
                    $book->authors()->sync($request->get('authors'));
                }

                $book->load(['authors']);

                return response()->json([
                    'success' => true,
                    'book' => $book,
                ]);
            });
        }
        catch (Exception $e) {
            Log::error('Failed to update Book #'.$id.': '.$e->getMessage().PHP_EOL.'Input: '.print_r($request->except(['token']), true).PHP_EOL.$e->getTraceAsString());

            return response()->json(['success' => false], 500);
        }
    }
}
