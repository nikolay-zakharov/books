<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

abstract class Controller extends \App\Http\Controllers\Controller
{
    /** @var User */
    protected $user;

    public function __construct()
    {
        try {
            $this->user = JWTAuth::parseToken()->authenticate();
        }
        catch (JWTException $e) {}
    }

    protected function getCurrentPage(Request $request)
    {
        return $request->get('page') ?: 1;
    }

    protected function getPageLength(Request $request)
    {
        return $request->get('items') ?: env('DEFAULT_LIST_PAGE_LENGTH', 10);
    }

    protected function getPaginated(Builder $query, Request $request, Closure $callback, $columns = ['*'])
    {
        $total_records = (clone $query)->toBase()->getCountForPagination();
        $page = $this->getCurrentPage($request);
        $page_size = $this->getPageLength($request);

        /** @var Model[]|Collection $items */
        $items = $total_records == 0
            ? new Collection()
            : $query->forPage($page, $page_size)->get($columns);

        return $callback($items, [
            'Total-Pages' => ceil($total_records/$page_size),
            'Total-Records' => $total_records,
            'Page' => $page,
            'Page-Size' => $page_size,
        ]);
    }

    protected function getEmptyPaginated(Request $request, Closure $callback)
    {
        $total_records = 0;
        $page = $this->getCurrentPage($request);
        $page_size = $this->getPageLength($request);

        return $callback([
            'Total-Pages' => ceil($total_records/$page_size),
            'Total-Records' => $total_records,
            'Page' => $page,
            'Page-Size' => $page_size,
        ]);
    }
}
