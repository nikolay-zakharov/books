<?php

namespace App\Http\Controllers\Api;

use App\Models\Author;
use Exception;
use Illuminate\Http\Request;
use Log;

class AuthorsController extends Controller
{
    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
        ]);

        try {
            /** @var Author $author */
            $author = Author::create($request->only(['name']));

            return response()->json([
                'success' => true,
                'author' => $author,
            ]);
        }
        catch (Exception $e) {
            Log::error('Failed to create Author: '.$e->getMessage().PHP_EOL.'Input: '.print_r($request->except(['token']), true).PHP_EOL.$e->getTraceAsString());

            return response()->json(['success' => false], 500);
        }
    }

}
